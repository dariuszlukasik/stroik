package com.dlukasik.stroik;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Bundle;


import android.app.Activity;
import android.view.View;
import android.util.Log;
import android.media.MediaRecorder;
import android.widget.TextView;
import android.widget.Toast;

import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;


public class MainActivity extends Activity {

    private static final String TAG = "stroik";
    private static AudioRecord mAudioRecord;
    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_STEREO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    short[] audioByteData = new short[mBufferSizeInBytes/2];
    double[] audioDoubleData = new double[mBufferSizeInBytes/2];
    double[] audioDoubleData2 = new double[mBufferSizeInBytes];
    DoubleFFT_1D fft;
    public TextView freqTextView;
    private static Activity thisActivity;
    private static float peakFreq;
    private static int mBufferSizeInBytes = 10*AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
            AudioFormat.CHANNEL_IN_STEREO ,AudioFormat.ENCODING_PCM_16BIT);
    int BufferElements2Rec = mBufferSizeInBytes/2;
    int BytesPerElement = 2;
    protected Runnable recordRunnable = new Runnable() {

        @Override
        public void run() {
            int readsize = 0;

            Log.d(TAG, "start to record");
            // start the audio recording
            try {
                mAudioRecord.startRecording();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

            while (isRecording == true) {

                readsize = mAudioRecord.read(audioByteData, 0, mBufferSizeInBytes/2);

                Log.d(TAG, "readsize: "+ readsize);
                for(int i = 0; i < audioByteData.length; i++ )  {
                   //Log.d(TAG, "audioByteData["+i+"]" + audioByteData[i]);
                    audioDoubleData[i] = (double)audioByteData[i];
                    audioDoubleData2[i] = audioDoubleData[i];
                    audioDoubleData2[i+1] = 0;

                }
                fft.complexForward(audioDoubleData2);
                double magnitude[] = calculateMagnitude(audioDoubleData2);
                peakFreq = calculatePeakFrequency(magnitude);
                Log.d(TAG, "MAX FREQ:" + peakFreq);
                freqTextView.post(new Runnable() {
                    @Override
                    public void run() {
                        freqTextView.setText("" + MainActivity.peakFreq);
                    }
                });
                if (AudioRecord.ERROR_INVALID_OPERATION != readsize) {
                }
            }

            // stop recording
            try {
                mAudioRecord.stop();
                mAudioRecord.release();

            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }


        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAudioRecord =new  AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);
         fft = new DoubleFFT_1D(mBufferSizeInBytes/2);
        Log.d(TAG , "MinBufferSize"+AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT) );
        freqTextView = (TextView) findViewById(R.id.freqTextView);
        thisActivity = this;
    }

    public   void onClickStartHandle(View v) {
        isRecording = true;

            Log.d(TAG, "StarClick");
            recordingThread = new Thread(recordRunnable);
            recordingThread.start();


    }

    public void onClickStopHandle(View v) {
        isRecording = false;

    }

    public  static double [] calculateMagnitude(double[] bufferDouble ) {
        double mag[] = new double[bufferDouble.length/2];
        for (int i = 0; i < (bufferDouble.length/2)-1; i++) {
            double real= bufferDouble[2*i];
            double imag=bufferDouble[2*i+1];
            mag[i] =Math.sqrt((real*real)+(imag*imag));
        }
        return mag;
    }

    public static float calculatePeakFrequency(double[] mag){
        double max_mag =0.0;
        int max_index = -1;
        for(int i=0;i<mag.length;i++){
            if(mag[i]>max_mag){
                max_mag = mag[i];
                max_index = i;
            }
        }
        Log.d(TAG, "maxindex"+ ": "+ max_index);
        return max_index* 2*RECORDER_SAMPLERATE/mBufferSizeInBytes;
    }


}
